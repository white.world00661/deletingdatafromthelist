import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Delete {

    public static void main(String args[]) {

        Set<String> mas1 = Collections.newSetFromMap(new ConcurrentHashMap<>());
        mas1.addAll(Arrays.asList("a", "b", "c", "f", "e", "f", "g"));

        Set<String> mas2 = Collections.newSetFromMap(new ConcurrentHashMap<>());
        mas2.addAll(Arrays.asList("e", "f", "g", "h", "f", "j"));

        for (String s : mas1) {
            if (check(s)) {
                mas1.remove(s);
            }
        }
        for (String s : mas2) {
            if (check(s)) {
                mas2.remove(s);
            }
        }

        System.out.println(mas1);
        System.out.println(mas2);
    }

    public static boolean check(String str) {
        return str.equals("f");
    }

}